package aivanz.brainfuck.app;

import java.util.Scanner;

import aivanz.brainfuck.app.compiler.Compiler;

public class App {
	public static void main(String args[]) {
		Compiler compiler = new Compiler();
		Scanner key = new Scanner(System.in);
		String input = "";

		System.out.println("Brainfuck compiler.");
		System.out.println("Press Enter at the end of code, then Ctrl+Z to compile.\n");
		System.out.println("Insert source code below:");
		input = key.nextLine();

		compiler.setSourceCode(input);

		try {
			compiler.compile();
		} catch (Exception e) {
			e.printStackTrace();
		}

		key.close();

	}
}
