package aivanz.brainfuck.app.compiler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * <b><i>Compiler</i></b>
 * 
 * <pre>
 * Compiler class.
 * This class compiles the source code of the application.
 * </pre>
 */
public class Compiler {
	private String source_code;
	private char validCharacters[] = { '+', '-', '[', ']', '<', '>', '.', ',' };

	public Compiler() {
		this.source_code = "";
	}

	public Compiler(String source_code) {
		this.source_code = source_code;
	}

	public String getSourceCode() {
		return this.source_code;
	}

	public void setSourceCode(String source_code) {
		this.source_code = source_code;
	}

	private String fixIntegrity(String input) {
		String result = "";

		for (int i = 0; i < input.length(); i++)
			for (int j = 0; j < validCharacters.length; j++)
				if (input.charAt(i) == validCharacters[j])
					result += input.charAt(i);

		return result;
	}

	public void compile() throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String source = fixIntegrity(this.source_code);
		int data[] = new int[8];

		// Main logic
		int ptr = 0;
		for (int i = 0; i < source.length(); i++) {
			switch (source.charAt(i)) {
			case '+':
				data[ptr] = (data[ptr] == 255) ? 0 : data[ptr] + 1;
				break;

			case '-':
				data[ptr] = (data[ptr] == 0) ? 255 : data[ptr] - 1;
				break;

			case '<':
				ptr = (ptr == 0) ? data.length - 1 : ptr - 1;
				break;

			case '>':
				ptr = (ptr == data.length - 1) ? 0 : ptr + 1;
				break;

			case '[':
				if (data[ptr] == 0) {
					int counter = 0;
					for (int j = i + 1; j < source.length(); j++) {
						if (source.charAt(j) == '[')
							counter++;
						else if (source.charAt(j) == ']') {
							if (counter > 0)
								counter--;
							else if (counter == 0) {
								i = j;
								break;
							}
						}
					}
					break;
				} else
					break;

			case ']':
				if (data[ptr] != 0) {
					int counter = 0;
					for (int j = i - 1; j > 0; j--) {
						if (source.charAt(j) == ']')
							counter++;
						else if (source.charAt(j) == '[') {
							if (counter > 0)
								counter--;
							else if (counter == 0) {
								i = j;
								break;
							}
						}
					}
					break;
				} else
					break;

			case '.':
				System.out.print((char) data[ptr]);
				break;

			case ',':
				try {
					System.out.print("Insert in position " + ptr + ": ");
					data[ptr] = br.readLine().charAt(0);
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;

			default:
				throw new Exception("Invalid char");
			}

		}

		System.out.println("\n");
		for (int i = 0; i < data.length; i++)
			System.out.println("[" + i + "]: " + data[i]);

	}

}
